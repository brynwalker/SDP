﻿using NUnit.Framework;
using System;
using MyGame;

namespace UnitTests
{
    [TestFixture]
    public class SpeedBoostTileUnitTests
    {
        SpeedBoostTile s;

        [SetUp]
        public void Init()
        {
            s = new SpeedBoostTile();
        }

        [Test]
        public void TestContainsSpeedBoost()
        {
        }

        [Test]
        public void TestSpeedUpPlayer()
        {}

        [Test]
        public void TestNoSpeedUpPlayer()
        {}

        [Test]
        public void TestSpeedUpEnemy()
        {}

        [Test]
        public void TestNoSpeedUpEnemy()
        {}

        [Test]
        public void TestIsTile()
        {}
    }
}